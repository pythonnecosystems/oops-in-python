# Python에서의 OOP (Part 2) <sup>[1](#footnote_1)</sup>

이 포스팅에서는 앞서 '[Python OOP의 순간](https://pythonnecosystems.gitlab.io/python-oops-moments)'에서 소개한 Part 1의 후속편으로 Python의 OOP에 대해 자세히 알아볼 것이다. 객체 지향 프로그래밍의 네 가지 기둥에 대해서는 이미 설명하였다. 여기서는 다양한 유형의 메서드, 데코레이터 등 객체 지향 프로그래밍의 다른 부분에 대해 설명한다. 이제 본격적으로 Python의 OOP에 대해 알아보자.

<a name="footnote_1">1</a>: [OOPS in Python — Part 2](https://medium.com/@shahooda637/oops-in-python-part-2-5af6a52e2a1a)를 편역한 것이다.
