# Python에서의 OOP (Part 2)

## 데코레이터(Decorators)

![](./images/1_qQEkbIiXtzuFbnEFz_t5iQ.webp)

Python에서 객체 지향 프로그래밍을 공부할 때는 [이전 글](https://pythonnecosystems.gitlab.io/python-oops-moments)에서 설명한 추상화, 캡슐화, 상속 및 다형성이라는 네 가지 기둥 외에도 많은 것을 배우게 될 것이다.

이 포스팅에서는 Python에서 oops로 배우는 추가 메서드와 함수 중 하나인 **데코레이터**에 대해 설명할 것이다.

Python에서 데코레이터는 다른 함수나 메서드의 동작을 수정하거나 확장하는 데 사용되는 특수한 유형의 함수이다. 데코레이터는 소스 코드를 직접 수정하지 않고도 함수나 메서드에 추가 기능을 만들기 위해 적용하거나 사용하는 경우가 많다.

### Python의 Decorator란?
데코레이터에 대한 간단한 정의는 다음과 같다. 데코레이터 함수는 다른 함수를 인수로 받아 새로운 함수를 반환하는 고차 함수이다. 이 새 함수는 일반적으로 원래 함수의 동작을 확장하거나 수정한다. 데코레이터 함수는 일반적으로 특수 문자 "@"가 앞에 붙으며 항상 데코레이션되는 함수 위에 배치된다.

다음 코드에서는 데코레이터의 몇 가지 예와 원래 소스 코드를 변경하는 함수를 확장하는 데 데코레이터를 사용하는 방법을 보이고 있다.

```python
 defining normal function

def test():
  print("The addition of two odd numbers 3 and 7")
  print(3 + 7)
  print("is always an even number")

test()
```

```
Output:
The addition of two odd numbers 3 and 7
10
is always an even number
```

```python
# defining a decorator function

def sum_decorator(func):
  def inner_deco():
    print("The addition of two odd numbers 3 and 7")
    func()
    print("is always an even number")
  return inner_deco
```

```python
# funtion without using decorator
def odd_add():
  print(3+7)

odd_add()
```

```
Output:
10
```

```python
# using a decorator to extend the functionality of a normal odd addition function

@sum_decorator
def odd_add():
  print(3+7)

odd_add()
```

```
Output:
The addition of two odd numbers 3 and 7
10
is always an even number
```

```python
#defining new decorator
def deco(func):
  def inner_deco():
    print("The addition odd numbers upto 10 is")
    func()
  return inner_deco

#using decorator on the function extending its functionality
@deco
def odd_add():
  sum = 0
  for i in range(10):
    if i%2 != 0:
      sum += i
  print(sum)

#calling the decorated function
odd_add()
```

```
Output:
The addition odd numbers upto 10 is
25
```

### OOP에서 데코레이터가 필요한 이유? 코딩 시 이점은?

1. **코드 재사용성**:
데코레이터는 개발자가 공통된 기능이나 동작을 여러 함수나 메서드에 적용할 수 있도록 함으로써 코드 재사용성을 높일 수 있다. 이 방법은 유사한 동작을 하는 클래스가 여러 개 있을 때 OOP로 코드를 작성하는 데 유용하다.

1. **관심사 분리**:
데코레이터는 개발자가 코드의 서로 다른 관심사나 측면을 분리하는 데 도움을 줄 수 있다. 예를 들어 로그인, 인증 등을 위한 데코레이터를 사용할 수 있다. 이렇게 각기 다른 작업에 대해 서로 다른 데코레이터를 사용하면 특히 협업 환경에서 작업할 때 코드베이스가 더 깔끔하고 유지 관리가 쉬워진다.

1. **개방형 또는 폐쇄형 원칙**:
데코레이터는 객체 지향 프로그래밍의 "열기/닫기 원칙"을 따르는데, 클래스나 함수는 확장을 위해 열려 있어야 하지만 수정을 위해 닫혀 있어야 한다는 원칙이다. 이러한 상황에서 데코레이터를 사용하면 소스 코드를 변경하지 않고도 함수나 메서드의 동작을 확장할 수 있다.

1. **손쉬운 유지 관리**:
데코레이터를 사용하면 코드베이스 전체를 변경하지 않고도 함수나 메서드의 동작을 한 곳에서 항상 업데이트할 수 있으므로 코드 관리 및 유지보수가 더 쉬워진다.

1. **단일 책임 원칙 촉진**:
데코레이터는 함수나 메서드에 추가 기능을 모듈화할 수 있도록 함으로써 각 함수나 메서드가 단일 책임을 갖도록 도울 수 있다.

1. **가독성**:
데코레이터는 함수나 메서드의 핵심 기능을 보조 기능과 분리하여 코드 가독성을 향상시킬 수 있다.

대체로 Python의 데코레이터는 함수와 메서드의 동작을 확장하고 수정할 수 있는 강력한 도구로, 클래스와 객체를 다루는 객체 지향 프로그래밍에서 특히 유용하다.

## 인스턴스 메소드

![](./images/1_wY4ekk_vd268HhCjVgSkng.webp)

Python의 인스턴스 메서드는 이름에서 알 수 있듯이 클래스의 인스턴스에서 동작하는 메서드 타입이다. 이러한 메서드는 클래스 내부에 정의되며 클래스의 특정 인스턴스와 관련된 작업이나 연산을 수행하기 위한 것이다.

인스턴스 메서드는 첫 번째 매개변수로 `self`를 통해 인스턴스 자체를 받아 인스턴스의 속성과 동작을 액세스하고 수정할 수 있다.

인스턴스 메서드의 구문은 간단하며 아래 코드에서 볼 수 있다.

```python
class student:

  #instance method
  def details(self, name, age, standard):
    self.name = name
    self.age = age
    self.standard = standard
```

인스턴스 메서드는 위 코드에서와 같이 `self` 키워드를 사용하여 클래스의 속성을 액세스한다.

`self.name` 코드는 인스턴스 메서드가 클래스의 `name` 어트리뷰트를 액세스하고 있음을 의미한다.

### 수정
인스턴스 메서드는 속성 값을 변경하여 인스턴스의 상태를 쉽게 수정할 수 있다. 인스턴스 메서드는 다른 인스턴스 메서드를 호출하거나 인스턴스와 관련된 다른 작업을 수행할 수도 있다.

### 행위(Behavior)
인스턴스 메서드는 클래스의 동작을 캡슐화하고 클래스의 인스턴스가 데이터와 상호 작용하고 연산을 수행하는 방법을 정의한다. 종종 클래스의 핵심 기능을 구현하기도 한다.

### 인스턴스에 바인딩
인스턴스 메서드를 호출하면 해당 메서드는 호출하는 인스턴스에 자동으로 바인딩된다. Python은 인스턴스를 암시적으로 자체 인수로 전달하므로 메서드를 호출할 때 인스턴스를 지정할 필요가 없다.

### 일반적인 사용 사례
인스턴스 메서드는 일반적으로 `__init__` 메서드에서 인스턴스 속성을 초기화하거나, 인스턴스별 계산을 수행하거나, 제어된 방식으로 인스턴스 속성을 수정하는 등의 작업에 사용된다.

다음 예는 인스턴스 메서드가 있는 클래스의 몇 가지 예를 보여주고 있다.

```python
class Dog:
    # defining the constructor method which is an instance method also
    def __init__(self, name, breed):
        self.name = name
        self.breed = breed
        self.energy = 100

    # the other instance method using the attributes from __init__ method
    def bark(self):
        print(f"{self.name} the {self.breed} barks loudly!")

    # the new instance method performing operations on its attributes
    def play(self, minutes):
        self.energy -= minutes    # self.energy is defined in __init__ as 100
        if self.energy < 0:
            self.energy = 0
        print(f"{self.name} has {self.energy}% energy left.")
```

```python
#defining the object
dog1 = Dog('Bruno', 'German Shephard')

#calling the bark method for dog1
dog1.bark()

# calling the play method for dog1
dog1.play(15)
```

```
Output:
Bruno the German Shephard barks loudly!
Bruno has 85% energy left.
```

인스턴스 메서드는 클래스의 객체와 관련된 동작과 동작을 정의할 수 있기 때문에 OOP의 기본 메서드이다. 앞으로 Python의 OOP에 존재하는 더 많은 타입의 메서드에 대해 알아보겠다.

## 클래스 메소드

![](./images/1_3tboqA5kmpt_CmP_b1ovJQ.webp)

여기서는 이미 Python OOP의 네 가지 기둥에 대해, 이 포스팅에서는 Python의 데코레이터에 대해 설명했다.

이제 Python OOP의 또 다른 개념인 클래스 메서드에 대해 알아보겠다.

Python에서 클래스 메서드는 클래스(객체)의 인스턴스가 아닌 클래스에 바인딩되는 메서드이다. 클래스 메서드는 `@classmethod` 데코레이터를 사용하여 정의되고, 클래스 자체를 첫 번째 인수로 받으며, 일반적으로 `cls`라는 이름을 사용한다.

클래스 메서드는 클래스의 특정 인스턴스가 아닌 클래스 전체와 관련된 연산에 자주 사용된다.

그러나 이것은 클래스 메서드의 공식적인 정의는 아니다. 자세한 설명은 다음 섹션에 자세히 설명한다.

## 클래스 메소드의 정의
클래스 메서드를 정의하려면 앞서 앞서 설명한 대로 클래스 정의 내에서 `@classmethod` 데코레이터를 사용한다. 메서드를 클래스 메서드로 만들려면 `@classmethod` 데코레이터를 사용하는 것 외에도 메서드의 첫 번째 매개변수로 `cls` 인수를 가져와야 한다.

클래스 이름을 통해 클래스 메서드에 직접 접근할 수 있고, 필요할 때마다 변수에 할당할 수 있으며, 해당 변수는 클래스에 정의된 다른 메서드에 접근할 수 있다.

클래스 메서드를 정의할 때는 `__init__` 메서드와 달리 메서드를 클래스로 직접 가져올 수 있으므로 항상 `self` 대신 `cls`를 사용한다.

다음에서 코드를 통해 개념을 설명한다.

```python
# defining a class

class school:

  # defining the constructor __init__() method of the class to enable the class to take the data
  def __init__(self, name, email):
    self.name = name
    self.email = email

  # defining a class method which overloads the __init__() method
  @classmethod
  def details(cls, name1, email1):
    return cls(name1, email1)

  # defining an instance method inside the class
  def school_details(self):
    print("This School was established in 1950s")
```

![](./images/0_JHEgWHRusM_1It4l.webp)

```python
# using the class method to add data into the class through constructor  

sch1 = school.details('Central School', 'centralschool@gmail.com')
```

![](./images/0_vRFQzlMQ8eo2FRbR.webp)

```python
sch1.email
```

```
Output:
centralschool@gmail.com
```

```python
sch1.name
```

```
Output:
Central School
```

```python
sch1.school_details()
```

```
Output:
This School was established in 1950s
```

위 코드에서 볼 수 있듯이 Python의 클래스 메서드를 사용하여 함수 오버로드를 수행할 수 있다. 위 코드에서 `__init__` 함수를 오버로드한 예시를 볼 수 있다. 하지만 함수 오버로딩이 정확히 무엇을 의미하는 것일까?

### 함수 오버로딩
함수 오버로딩은 동일한 범위에서 동일한 이름을 가진 여러 함수를 서로 다른 매개변수 목록으로 정의할 수 있는 기능이다. 함수에 전달된 인수의 수 또는 타입에 따라 호출할 함수가 선택된다.

함수 오버로딩은 다형성의 한 형태로, 같은 이름을 가진 여러 함수가 매개변수에 따라 다르게 작동할 수 있다.

### 정적 변수와 클래스 메서드
클래스 메서드 변수를 통해 클래스 내부에 정의된 정적 변수를 액세스할 수도 있다. 그 예제는 아래와 같다.

```python
class bank:

  def __init__(self, account_number, balance):
    self.account_number = account_number
    self.balance = balance
  
  mobile = 7089251232

  @classmethod
  def change_mobile(cls, mobile_num):
    bank.mobile = mobile_num

  @classmethod
  def details(cls, account_number, balance):
    return cls(account_number, balance)
  
  def customer_details(self):
    print('account number: ' , self.account_number)
    print('balance: ', self.balance)
    print('Mobile: ', bank.mobile)
```

인스턴스 메서드 내에서 정적 변수를 호출하는 것은 클래스 내부와 메서드를 통해 정적 변수를 사용하는 방법이다. 객체와 클래스 메서드 변수를 통해서도 접근할 수 있다.

또한 다음과 같이 클래스 메서드를 정의하면 클래스 내부에 정의된 정적 메서드의 값을 변경하는 데에도 사용할 수 있다.

```python
@classmethod
def change_mobile(cls, mobile_num):
  bank.mobile = mobile_num
```

```python
cus1 = bank.details('1287655825665', '1153225')
```

![](./images/0_tz39mSVH7LdGLmar.webp)

```python
cus1.customer_details()

Output:
account number:  1287655825665
balance:  1153225
Mobile:  7089251232
```

```python
cus1.mobile
```

```
Output:
7089251232
```

```python
cus1.mobile
```

```
Output:
4535786624
```

### Python OOP에서 클래스 메서드의 용도와 이점

1. **클래스 수준 데이터 접근과 수정하기**:
클래스 메서드는 클래스 수준 데이터 또는 속성을 액세스하고 수정하는 데 특히 유용하다. 클래스의 모든 인스턴스 간에 공유되는 데이터를 조작하거나 관리할 수 있다.

1. **팩토리 메서드**:
클래스 메서드는 클래스의 인스턴스를 생성하고 반환하는 데 사용할 수 있으며, 팩토리 메서드 역할을 한다. 이는 객체 생성을 커스터마이즈하거나 특정 제약 조건을 적용하거나 캐시된 인스턴스를 반환하려는 경우에 유용하다.

1. **대체 생성자**:
클래스 메서드는 클래스의 대체 생성자를 제공하는 데 사용할 수 있다. 예를 들어, 다른 데이터 타입이나 소스에서 인스턴스를 초기화하는 클래스 메서드를 만들 수 있다.

1. **싱글톤 패턴**:
클래스 메서드는 싱글톤 패턴을 구현하여 프로그램 전체에 클래스의 인스턴스가 하나만 존재하도록 하는 데 사용할 수 있다.

1. **네임스페이스 구성**:
클래스 메서드는 클래스 내에서 관련 메서드를 함께 그룹화하여 네임스페이스를 구성하는 데 도움이 될 수 있다.

1. **코드 가독성 향상**:
클래스 메서드는 메서드가 클래스 수준에서 작동한다는 것을 명시적으로 나타내어 메서드가 인스턴스 특정 상태에 의존하지 않는다는 것을 명확히 함으로써 코드 가독성을 향상시킬 수 있다.

1. **테스트와 모킹**:
단위 테스트에서 클래스 메서드는 인스턴스별 상태에 의존하지 않기 때문에 모킹과 테스트가 더 쉬워질 수 있다.

요약하면, Python의 클래스 메서드는 클래스 자체, 클래스 수준 데이터 또는 대체 생성자 제공과 관련된 연산에 유용하다. 클래스 메서드는 코드를 정리하고 가독성을 개선하며 객체 지향 프로그래밍 패러다임 내에서 클래스별 동작과 속성을 유연하게 관리할 수 있도록 도와준다.

## 정적 메서드(Static Method)

![](./images/1_6uqwnZ-0MQMYyFJEDpeX-g.webp)

Python의 객체 지향 프로그래밍에서 정적 메서드는 클래스의 인스턴스나 객체가 아닌 클래스에 속하는 메서드이다.

정적 메서드는 `@staticmethod` 데코레이터를 사용하여 정의되고, 인스턴스별 데이터나 메서드에 액세스할 수 없다. 정적 메서드는 일반적으로 특정 인스턴스가 아닌 클래스 전체와 관련된 연산에 사용된다. 정적 메서드는 유틸리티 함수, 헬퍼 메서드 또는 인스턴스 상태에 의존하지 않는 연산에 자주 사용된다.

이 섹션에서는 Python의 정적 메서드, Python 객체 지향 프로그래밍(OOP)에서의 용도와 장점에 대해 설명한다.

### 정적 메소드 정의
정적 메서드를 정의하려면 위에서 이미 설명한 대로 클래스 정의 내에서 데코레이터 `@staticmethod`를 사용한다.

일반 인스턴스 메서드나 클래스 메서드와 달리 정적 메서드는 첫 번째 인자로 각각 `self` 또는 `cls` 매개변수를 사용하지 않는다. 실제로 정적 메서드는 기본적으로 특별한 첫 번째 인자를 받지 않는다.

다음 코드는 정적 메서드를 정의하고 클래스에서 그리고 Python OOP의 객체를 통해 사용하는 방법을 설명한다.

```python
# defining a class

class travel:

  def traveller(self, name, place):
    self.name = name
    self.place = place

  flight_num = '23552'

  @staticmethod
  def travel_date(travel_date):
    print(travel_date)

  @classmethod
  def flight_details(cls, flight_number):
    travel.flight_num = flight_number
    return flight_number
```

```python
t1 = travel()
t1.traveller('Isha Choudhary', 'Bombay')
t1.name
```

```
Output
'Isha Choudhary'
```

```python
t1.place
```

```
Output:
'Bombay'
```

```python
#accessing static method

t1.travel_date('27/10/23')
```

```
Output:
27/10/23
```

```python
t1.flight_details(12535)
```

```
Output:
12535
```

> **Note**: 정적 메서드는 클래스 메서드 내부에서도 사용할 수 있다. 또한 하나의 정적 메서드를 다른 정적 메서드에서 사용할 수도 있다.

다음 코드는 그 예를 보여준다.

```python
class datascience_class:
  def student_details(self, name, mail_id, number):
    print(name, mail_id, number)

  @staticmethod
  def mentor_mail_id(mail_id):
    print(mail_id)

  # static method using another static method inside it
  @staticmethod
  def mentor_class(list_mentor):
    print(list_mentor)
    datascience_class.mentor_mail_id(['isha@gmail.com', 'sha@gmail.com'])

  @classmethod
  def class_name(cls, class_name):
    cls.mentor_class(['isha', 'krish'])

  # instance method using static method with one other static method inside it
  def mentor(self, mentor_list):
    print(mentor_list)
    self.mentor_class(['intro class', 'machine learning'])
```

```python
s1 = datascience_class()
```

```python
s1.mentor_class(['isha', 'sha'])
```

```
Output:
['isha', 'sha']
['isha@gmail.com', 'sha@gmail.com']
```

```python
s1.mentor(['isha', 'sha'])
```

```
Output:
['isha', 'sha']
['intro class', 'machine learning']
['isha@gmail.com', 'sha@gmail.com']
```

### OOP의 Static Methods 용도와 장점

1. **조직**:
정적 메서드는 클래스 내에서 관련 유틸리티 함수나 헬퍼 메서드를 그룹화하여 코드베이스를 보다 체계적이고 모듈식으로 만드는 데 사용된다.

1. **인스턴스 종속성 없음**:
정적 메서드는 인스턴스 상태에 의존하지 않는다. 인스턴스를 생성하지 않고도 클래스 자체에서 호출할 수 있으므로 인스턴스별 데이터로 작업할 필요가 없을 때 유용할 수 있다.

1. **코드 재사용성**:
정적 메서드는 코드 복제 없이 여러 위치에서 호출할 수 있으므로 코드 재사용성을 높여준다.

1. **네임스페이스**:
클래스 범위 내에 관련 메서드를 유지하여 이름 충돌을 방지함으로써 네임스페이스를 정리하는 데 도움이 된다.

1. **가독성 향상**:
정적 메서드는 메서드가 인스턴스 상태를 수정하지 않고 클래스 수준의 범위를 가짐으로써 코드 가독성을 향상시킨다.

1. **성능**:
경우에 따라 정적 메서드는 인스턴스 데이터 액세스의 오버헤드가 없기 때문에 인스턴스 메서드보다 약간 더 나은 성능을 제공할 수 있다.

1. **단위 테스트**:
정적 메서드는 인스턴스를 만들 필요 없이 개별적으로 쉽게 테스트할 수 있다. 테스트 중심 개발에서 단위 테스트와 모킹에 유용하다.

요약하면, Python의 정적 메서드는 클래스와 연관되어 있지만 인스턴스별 데이터에 의존하지 않는 메서드를 정의하는 데 사용된다. 정적 메서드는 코드 정리, 재사용성 및 가독성을 향상시켜 객체 지향 프로그래밍의 맥락에서 유틸리티 함수와 클래스 수준 작업을 더 쉽게 수행할 수 있다.

## 정적 (마법(Magic)/던더(Dunder)) 메서드

![](./images/1_FkkRQxcQTMOg4XkqSCgN0w.webp)

던더 메소드는 이름 앞뒤에 두 개의 밑줄이 있는 마법 메소드 또는 특수 메소드이다. 이름 앞에 밑줄이 두 번 붙어 있기 때문에 던더 메서드라고 부른다.

이러한 메서드는 특정한 의미를 가지며 클래스의 객체가 다양한 상황에서 어떻게 동작하는지를 정의하는 데 사용된다.

던더 메서드의 몇몇 예는 다음과 같다.

1. `__init__`: 클래스 인스트럭터로 사용한다.
1. `__str__`: `str()`으로 사용한다.
1. `__add__`: `+`로 사용한다.
1. `__repr__`: 객체의 명확한 문자열 표현을 반환하기 위해 `repr()`로 사용한다. 일반적으로 디버깅과 개발에 사용된다.
1. `__len__`: 객체의 길이를 확인하는 `len()` 함수로 사용한다.

Python에는 이와 같은 메서드가 더 많이 있다.

던더 메서드는 Python 객체 지향 프로그래밍의 기본 요소로, 필요에 따라 객체의 동작을 커스터마이즈할 수 있다. 이러한 메서드를 사용하면 객체를 더욱 강력하고 직관적으로 만들 수 있다.

## 속성 데코레이터 (Getters, Setters 및 Deleters)

![](./images/1_RfZez0Ctp7lP3C6iH-vWvQ.webp)

Python의 "속성 데코레이터"는 객체의 속성 또는 속성에 대한 액세스, 수정 및 삭제를 제어하는 특수 메서드를 정의하기 위한 내장 메커니즘이다.

속성 데코레이터를 사용하면 속성 값을 '가져오고', '설정하고', '삭제'하는 동작을 커스터마이즈할 수 있으며, 계산된 속성의 구현과 데이터 유효성 검사를 수행할 수 있다.

이 데코레이터는 일반적으로 아래에 정의된 대로 세 가지 메서드, 즉 `getter`, `setter` 및 `deleter`와 함께 사용된다.

1. **Getter 메서드**: 
Getter 메서드는 속성의 값을 가져오거나 검색하는 역할을 한다. 이 메서드는 `@property` 데코레이터를 사용한다. 값을 검색하기 위해 인스턴스 속성을 액세스할 때마다 `getter` 메서드가 자동으로 호출된다.
1. **Setter 메서드**: 
이 메서드는 속성의 값을 설정하거나 할당하는 역할을 한다. `@attribute_name.setter` 데코레이터를 사용한다. 인스턴스의 속성에 새로운 값이 할당되면 설정자 메서드가 자동으로 호출되어 할당 처리를 수행한다.

1. **Deleter 메서드**: 
Deleter 메서드는 이름에서 알 수 있듯이 어트리뷰트를 삭제하는 역할을 한다. 이 메서드는 데코레이터 `@attribute_name.deleter`를 사용한다. deleter 메서드는 `del` 문으로 인스턴스의 어트리뷰트를 삭제할 때마다 자동으로 호출된다.

다음 코드는 이러한 속성 데코레이터에 대한 몇몇 예를 보이고 있다.

```python
#defining the class
class Circle:
    def __init__(self, radius):
        self._radius = radius  # Private attribute with a leading underscore

    #defining first property decorator
    @property
    def radius(self):
        return self._radius

    #defining setter property for recently created property @radius
    @radius.setter
    def radius(self, value):
        if value < 0:
          #defining negative value error
            raise ValueError("Radius cannot be negative")
        self._radius = value

    @radius.deleter
    def radius(self):
        print("Deleting the radius attribute")
        del self._radius

    @property
    def area(self):
        return 3.14159 * self._radius ** 2
```

```python
# Creating an instance of Circle
circle = Circle(5)
```

```python
# Accessing the radius and area properties
print(circle.radius)  # Calls the getter method
print(circle.area)    # Calls the area property
```

```
Output:
5
78.53975
```

```python
# Setting the radius property
circle.radius = 7  # Calls the setter method
```

```python
# Deleting the radius property
del circle.radius  # Calls the deleter method
```

```
Output:
Deleting the radius attribute
```

'속성' 데코레이터를 사용하면 커스터마이징 동작이 있는 속성을 가진 클래스를 만들 수 있다. 이렇게 하면 코드가 더 견고해지고 유지 보수가 쉬워진다.

'속성' 데코레이터는 데이터 유효성 검사, 계산된 속성 또는 속성 액세스 제어를 클래스에 추가해야 할 때 특히 유용하다.

지금까지 Python에 있는 메서드와 데코레이터에 대해 알아보았다. Part 2에서는 모든 주제에 대한 설명과 코딩 예가 포함된 Python의 객체 지향 프로그래밍에 대해 설명한다.
